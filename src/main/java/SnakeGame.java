import config.GameBoard;

public  class SnakeGame {

    public static void main(String[] args) throws InterruptedException {

        GameBoard board = new GameBoard();
        board.init();
        board.run();

    }

}
