package game;

import config.Unit;

public interface Element {
    void draw(Unit unit);
}
